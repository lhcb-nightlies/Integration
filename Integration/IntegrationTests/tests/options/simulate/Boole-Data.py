
from Gaudi.Configuration import *
from Configurables import Boole, OutputStream

EventSelector().Input = ["DATAFILE='PFN:GaussTest.sim' TYP='POOL_ROOTTREE' OPT='READ'"]

Boole().Outputs  = ["DIGI","MDF"] # Test both output types
Boole().UseSpillover = True
#Boole().DigiType = "Extended"

OutputStream("RawWriter").Output = "DATAFILE='PFN:BooleTest.mdf' SVC='LHCb::RawDataCnvSvc' OPT='REC'"

OutputStream("DigiWriter").Output = "DATAFILE='PFN:BooleTest.digi' TYP='POOL_ROOTTREE' OPT='REC'"

