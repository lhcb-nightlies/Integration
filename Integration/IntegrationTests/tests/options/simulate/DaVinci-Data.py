
from Gaudi.Configuration import *
from Configurables import DaVinci

EventSelector().Input = ["DATAFILE='PFN:BrunelTest.dst' TYP='POOL_ROOTTREE' OPT='READ'"
                         ]

DaVinci().InputType = "DST" 
DaVinci().Simulation   = True
DaVinci().DataType = "2009"

