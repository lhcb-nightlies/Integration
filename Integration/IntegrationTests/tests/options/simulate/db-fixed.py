from Gaudi.Configuration import *
from Configurables import LHCbApp

LHCbApp().DDDBtag   = "head-20091211"
LHCbApp().CondDBtag = "sim-20091211-vc15mm-md100"
