import Configurables

if len(Configurables.__all__) <1000:
    print "Not enough configurables found, should be >1000"
    print sorted(Configurables.__all__)

for conf in sorted(Configurables.__all__):
    #usually should print out conf here, but not in integration!
    #print conf
    exec("Configurables.%s()" % conf)

print "done"

